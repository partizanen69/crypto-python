import time
from configparser import ConfigParser
import json5
import os

import shared_lib as lib

class Options():
    def __init__(self):
        self.sql_lib = {}
        self.config = {}

        self.env = 'production' if os.getenv('ENV') == 'production' else 'development'
        print(f'Starting the app in {self.env.upper()} mode')

        self.generate_sql_lib()
        self.generate_options()
    
    def generate_sql_lib(self):
        print('Generating sql library')
        start = time.time()

        config = ConfigParser(allow_no_value=True)
        config.read('./config/sql.ini')

        for section in config.sections():
            
            for option in config.options(section):
                query = config.get(section, option)
                query = query.replace('\n', ' ')
                
                self.sql_lib[option] = query
        
        print(f'Sql library generated within {lib.calc_time_ms(start)} ms')


    def generate_options(self):
        with open('./config/config.json5', 'r') as reader:
            config = reader.read()
        
        self.config = json5.loads(config)

        if self.env == 'development':
            return
        
        with open('./config/production.json5', 'r') as reader:
            prod_config = reader.read()

        prod_config = json5.loads(prod_config)
        
        for item in prod_config:
            self.config[item] = prod_config[item]
        

options = Options()