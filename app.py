import tornado.ioloop
import tornado.web

from options import options
from mysql_db import mysql_db
from binance import binance


class MainHandler(tornado.web.RequestHandler):
    async def get(self):
        self.write("Hello, world")

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])

def main():
    pass
    # binance.make_request()
    

if __name__ == "__main__":
    app = make_app()
    main()
    app.listen(5000)
    print('Server is running on port 5000')
    tornado.ioloop.IOLoop.current().start()

