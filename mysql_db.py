import aiomysql
import asyncio
from tornado import ioloop
from operator import itemgetter

from options import options

class Mysql_db():
    def __init__(self):
        self.pool = None
        
        current_loop = ioloop.IOLoop.current()
        current_loop.add_callback(self.establish_connection)

    async def establish_connection(self):
        current_loop = ioloop.IOLoop.current().asyncio_loop

        con = options.config['db_connection']
        host, port, user, password, db = itemgetter('host', 'port', 'user', 'password', 'db')(con)

        self.pool = await aiomysql.create_pool(
            host = host, 
            port = port,
            user = user,
            password = password,
            db = db,
            loop = current_loop,
            autocommit = True
        )

        print('Connection has been established with the database')


    async def read(self):
        pass

    async def execute(self, query_name, params = []):
        if isinstance(query_name, str) == False or query_name == '':
            raise Exception('Query arg must be a string with query name')

        if isinstance(params, list) == False:
            raise Exception('Params arg must be a list')
        
        if query_name not in options.sql_lib:
            raise Exception(f'Query {query_name} was not found in sql_lib')

        if len(params) > 0:
            query_text = self.apply_params(options.sql_lib[query_name], params)
        
        conn = await self.pool.acquire()
        cur = await conn.cursor()
        await cur.execute(query_text)
        self.pool.release(conn)


    async def bulk_insert(self):
        pass
    

    """ 
    @param query_content {String}
    @param params {List of dictionaries} { param: '{param_name}', value: 123 }
    """
    def apply_params(self, query_content, params):
        for p in params:
            if 'param' not in p or 'value' not in p:
                continue
            
            if p['param'][0] == '{' and p['param'][-1] == '}':
                value = str(p['value'])
                query_content = query_content.replace(p['param'], value)
            
            if p['param'][0] == '[' and p['param'][-1] == ']':
                if p['value'] == 'all':
                    query_content = query_content.replace(p['param'], '<> \'-1\'')
                elif isinstance(p['value']) == list:
                    vals = ', '.join(p['value'])
                    query_content = query_content.replace(p['param'], f'in ({vals})')
                    
        return query_content

mysql_db = Mysql_db()