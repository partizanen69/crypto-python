from tornado.httpclient import AsyncHTTPClient
from operator import itemgetter
import time

async def make_request(options):
    url = itemgetter('url')(options)
    
    http_client = AsyncHTTPClient()
    
    try:
        response = await http_client.fetch(url)
    except Exception as err:
        return { 'err': repr(err) }
    else:
        return { 'result': response.body }
    
def try_make_float(string):
    try:
        num = float(string)
        return num
    except Exception as err:
        return False

def calc_time_ms(start_time):
    return round((time.time() - start_time) * 1000, 5)