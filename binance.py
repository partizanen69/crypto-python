from tornado import ioloop
from tornado import gen
from tornado.websocket import websocket_connect
import asyncio
import json
import math
import time
from operator import itemgetter

import shared_lib as lib
from mysql_db import mysql_db

# from shared_lib import make_request
# from shared_lib import lib.try_make_float

class Binance():
    def __init__(self):
        self.endpoint = 'https://api.binance.com'
        self.symbols = {} # { BTCETH: { 'baseAsset': 'BTC', quoteAsset: 'ETH' }, ... }
        self.ws = None
        self.rates = {} # { BTC: { USDT: { price: 9365.43, qty: 0.266915, type: 'bid' }, ...  }, ... }
        self.triangles = [] # [ ['ETH', 'BTC', 'LTC'], ['ETH', 'BTC', 'BNB'], ... ]
        self.squares = []
        self.kaka = 'asdasd'
        
        # get symbols reference once
        current_loop = ioloop.IOLoop.current()
        current_loop.add_callback(self.get_symbols_info)
        current_loop.add_callback(self.connect_to_socket)
        current_loop.add_callback(self.get_initial_rates)

        # calculate arbitrage opportunities once in n seconds 
        ioloop.PeriodicCallback(self.find_profitable_triangles, 1000 * 1).start() # once per 1 seconds
        ioloop.PeriodicCallback(self.find_profitable_squares, 1000 * 15).start() # once per 5 seconds
        ioloop.PeriodicCallback(self.identify_arbitrage_chains, 1000 * 60 * 60 * 4).start() # once per 4 hours


    async def get_symbols_info(self):
        def fill_in_symbols_ref():
            for symb in data['symbols']:
                key = symb['symbol']
                self.symbols[key] = { 
                    'baseAsset': symb['baseAsset'],
                    'quoteAsset': symb['quoteAsset'],
                }
        
        print('Getting symbols info')
        start_time = time.time()

        print('Reading symbols from binance_symbols.json')
        with open('./data/binance_symbols.json', 'r') as reader:
            data = reader.read()

        try:
            data = json.loads(data)
        except Exception as err:
            return print('get_symbols_info could not load json data', str(err))

        # fill in self.symbols reference
        if 'symbols' in data:
            fill_in_symbols_ref()

        print(f'Got {len(self.symbols.keys())} symbols from binance_symbols.json')

        print('Getting symbols info from Binance API')
        options = {
            'url': f'{self.endpoint}/api/v3/exchangeInfo',
        }
        data = await lib.make_request(options)

        if 'err' in data:
            return print(f'Could not get symbols info from Binance API', str(data['err']))
            
        try:
            data = json.loads(data['result'])
        except Exception as err:
            return print('get_symbols_info could not load json data', str(err))

        if 'symbols' not in data or type(data['symbols']) is not list or len(data['symbols']) == 0:
            return print('Invalid Binance symbols info')
        
        symb = data['symbols'][0]
        if type(symb['baseAsset']) != str or type(symb['quoteAsset']) != str or type(symb['symbol']) != str:
            return print('Invalid Binance symbols info')

        with open('./data/binance_symbols.json', 'w') as writer:
            writer.write(json.dumps(data))
            
        print(f'Symbols info received from binance ({len(self.symbols.keys())} symbols in total) and recorded ' + 
            f'in the binance_symbols.json within {lib.calc_time_ms(start_time)} ms')


    async def connect_to_socket(self):
        try:
            self.ws = await websocket_connect(
                'wss://stream.binance.com:9443/ws/!bookTicker', 
                on_message_callback = self.update_rates
            )
        except Exception as err:
            print('Could not connect to Binance wss:', str(err))
        finally:
            print('Succesully connected to Binance wss')
    

    async def get_initial_rates(self):
        print('Getting initial rates info from Binance')
        options = {
            'url': f'{self.endpoint}/api/v3/ticker/bookTicker',
        }
        data = await lib.make_request(options)

        if 'err' in data:
            return print('Could not get initial rates from Binance', str(data['err']))

        try:
            data = json.loads(data['result'])
        except Exception as err:
            return print('Could not parse initial rates json data from Binance', err)
        
        for ticker in data:
            if 'symbol' not in ticker or type(ticker['symbol']) != str:
                print('Incorrect symbol string in ticker', ticker)
                continue
            
            if 'askPrice' not in ticker or 'bidPrice' not in ticker:
                print('No ask price or bidPrice in ticker', ticker)
                continue

            symb = ticker['symbol']

            if symb not in self.symbols:
                print(f'{symb} was not found in self.symbols')
                continue
            
            baseAsset = self.symbols[symb]['baseAsset']
            quoteAsset = self.symbols[symb]['quoteAsset']

            ask_price = lib.try_make_float(ticker['askPrice'])
            bid_price = lib.try_make_float(ticker['bidPrice'])

            if isinstance(ask_price, float) == False or isinstance(bid_price, float) == False:
                print('Invalid askPrice or bidPrice in ticker', ticker)
                continue
                
            ask_qty = lib.try_make_float(ticker['askQty'])
            bid_qty = lib.try_make_float(ticker['bidQty'])

            if isinstance(ask_qty, float) == False or isinstance(bid_qty, float) == False:
                print('Invalid askQty or bidQty in ticker', ticker)
                continue
            
            if baseAsset not in self.rates:
                self.rates[baseAsset] = {}
            self.rates[baseAsset][quoteAsset] = { 'price': bid_price, 'qty': bid_qty, 'type': 'bid' }

            if quoteAsset not in self.rates:
                self.rates[quoteAsset] = {}
            self.rates[quoteAsset][baseAsset] = {
                'price': 0 if ask_price == 0 else 1 / ask_price,
                'qty': ask_qty,
                'type': 'ask'
            }
            
        print('Initial rates was received from Binance and recorded in self.rates')
        self.identify_arbitrage_chains()


    """ 
        Expect msg
        'u': 1928453061, // order book updateId
        's': 'BTCUSDT', // symbol
        'b': '9381.02000000', // best bid price # bid - they offer to buy from you your base asset
        'B': '1.44022500', // best bid qty
        'a': '9381.82000000', // best ask price # ask - they offer to sell to you their base asset
        'A': '0.26648400' // best ask qty
    """
    def update_rates(self, msg):
        if isinstance(msg, str):
            msg = json.loads(msg)
        else:
            return print('Unexpected socket msg format received from Binance', msg)
        
        if 's' not in msg or type(msg['s']) != str:
            return print('Got rate msg with incorrect symbol', msg)
        
        symb = msg['s']
        if symb not in self.symbols:
            return print(f'{symb} symbol was not found in self.symbols')

        quoteAsset = self.symbols[symb]['quoteAsset']
        baseAsset = self.symbols[symb]['baseAsset']

        bid_price = lib.try_make_float(msg['b'])
        ask_price = lib.try_make_float(msg['a'])
        if isinstance(bid_price, float) == False or isinstance(ask_price, float) == False:
            return print('Could not convert bid or ask price to float', msg)
        
        bid_qty = lib.try_make_float(msg['B'])
        ask_qty = lib.try_make_float(msg['A'])
        if isinstance(bid_qty, float) == False or isinstance(ask_qty, float) == False:
            return print('Could not convert bid or ask qty to float', msg)

        if baseAsset not in self.rates:
            self.rates[baseAsset] = {}
        self.rates[baseAsset][quoteAsset] = { 'price': bid_price, 'qty': bid_qty, 'type': 'bid' }

        if quoteAsset not in self.rates:
            self.rates[quoteAsset] = {}
        self.rates[quoteAsset][baseAsset] = { 
            'price': 0 if ask_price == 0 else 1 / ask_price, 
            'qty': ask_qty, 
            'type': 'ask' 
            }


    def identify_arbitrage_chains(self):
        print('Start idenfying possible arbitrage chains')
        start_time = time.time()
        
        self.triangles = []
        self.squares = []

        for init_coin in self.rates:

            for second_coin in self.rates[init_coin]:

                for third_coin in self.rates[second_coin]:
                    if third_coin == init_coin:
                        continue

                    if init_coin in self.rates[third_coin]:
                        self.triangles.append([ init_coin, second_coin, third_coin ])

                    for fourth_coin in self.rates[third_coin]:
                        if fourth_coin == init_coin or fourth_coin == second_coin:
                            continue

                        if init_coin in self.rates[fourth_coin]:
                            self.squares.append([ init_coin, second_coin, third_coin, fourth_coin ])
        
        print(f'Possible arbitrage chains identified within {lib.calc_time_ms(start_time)} ms.' + 
            f'We have {len(self.triangles)} triangles and {len(self.squares)} squares')


    def find_profitable_triangles(self):
        print('Start finding profitable chains for triangles')
        start_time = time.time()

        prof_triangles = []
        for chain in self.triangles:
            try:
                first_price = self.rates[chain[0]][chain[1]]['price']
                second_price = self.rates[chain[1]][chain[2]]['price']
                third_price = self.rates[chain[2]][chain[0]]['price']
            except Exception as err:
                print('Could not find rates for the triangle chain', chain)
                continue

            result = 100 * first_price * second_price * third_price - 100
            if result > 0.3:
                prof_triangles.append(chain + [result])
        
        print(f'Finding profitable chains for {len(self.triangles)} triangles finished ' +
            f'within {lib.calc_time_ms(start_time)} ms')
        
        # prof_triangles = [['BTC', 'ETH', 'AMB', 0.42714099167677944]]
        if len(prof_triangles) > 0:
            print('Found profitable triangles', prof_triangles)
            current_loop = ioloop.IOLoop.current()
            current_loop.add_callback(self.save_prof_chain_into_db, prof_triangles)


    def find_profitable_squares(self):
        print('Start finding profitable chains for squares')
        start_time = time.time()
    
        prof_squares = []
        for chain in self.squares:
            try:
                first_price = self.rates[chain[0]][chain[1]]['price']
                second_price = self.rates[chain[1]][chain[2]]['price']
                third_price = self.rates[chain[2]][chain[3]]['price']
                fourth_price = self.rates[chain[3]][chain[0]]['price']
            except Exception as err:
                print('Could not find rates for the square chain', chain)
                continue

            result = 100 * first_price * second_price * third_price * fourth_price - 100
            if result > 0.4:
                prof_squares.append(chain + [result])

        print(f'Finding profitable chains for {len(self.squares)} squares finished ' +
            f'within {lib.calc_time_ms(start_time)} ms')
        
        if len(prof_squares) > 0:
            print('Found profitable squares', prof_squares)
            current_loop = ioloop.IOLoop.current()
            current_loop.add_callback(self.save_prof_chain_into_db, prof_squares)

    async def save_prof_chain_into_db(self, chains):
        print(f'Saving {len(chains)} profitable arbitrage chains into db')
        start = time.time()

        query = 'record_arbitrage_history'
        for chain in chains:
            otype = 't' if len(chain) == 4 else 's'
            
            margin = chain[3] if otype == 't' else chain[4]
            margin_perc = round(margin, 2)

            # form details_1 value
            def form_details_value(coin_1, coin_2):
                symbol = f'{coin_1}{coin_2}'
                if symbol not in self.symbols:
                    symbol = f'{coin_2}{coin_1}'
                
                rate = self.rates[coin_1][coin_2]
                price, qty, ptype = itemgetter('price', 'qty', 'type')(rate)
                # symbol / price / qty / type (bid / ask)
                price = '{:.8f}'.format(round(price, 8))
                qty = '{:.8f}'.format(round(qty, 8))
                
                return f'{symbol} / {price} / {qty} / {ptype}'

            details_1 = form_details_value(chain[0], chain[1])
            details_2 = form_details_value(chain[1], chain[2])
            
            if otype == 't':
                details_3 = form_details_value(chain[2], chain[0])
                details_4 = 'null'
            elif otype == 's':
                details_3 = form_details_value(chain[2], chain[3])
                details_4 = form_details_value(chain[3], chain[0])

            params = [
                { 'param': '{exchange_id}', 'value': 1 },
                { 'param': '{margin_perc}', 'value': margin_perc },
                { 'param': '{type}', 'value': otype },
                { 'param': '{init_coin}', 'value': chain[0] },
                { 'param': '{sec_coin}', 'value': chain[1] },
                { 'param': '{third_coin}', 'value': chain[2] },
                { 'param': '{fourth_coin}', 'value': 'null' if otype == 't' else chain[3] },
                { 'param': '{details_1}', 'value': details_1 },
                { 'param': '{details_2}', 'value': details_2 },
                { 'param': '{details_3}', 'value': details_3 },
                { 'param': '{details_4}', 'value': details_4 },
            ]

            try:
                await mysql_db.execute(query, params)
            except Exception as err:
                print('Could not save profitable chain into db because of err', err)

        print(f'Succesfully saved {len(chains)} profitable chains into db within {lib.calc_time_ms(start)}')



binance = Binance()